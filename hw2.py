#Casier in the cinema
viewer_age = input ("Enter your age:")
if not viewer_age:
    print ("Please, write something")
elif not viewer_age.isdecimal():
    print("Please, type only numbers next time")
elif viewer_age.count(viewer_age[0]) == len(viewer_age) and viewer_age.count(viewer_age[0]) != 1:
    print ("What a peculiar age!")
elif int(viewer_age) < 7:
   print ("Where are your parents?")
elif int(viewer_age) < 16:
   print ("It's an adult's movie!")
elif int(viewer_age) > 65:
   print ("Show me your passport!")
else:
   print ("There are no more tickets!")

