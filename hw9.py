import datetime

class Human:
    population = 0
    

def __init__(self, 
            firstname: str,
            lastname: str,
            age: int, 
            sex: str, 
            birth_date=datetime.date.today()): self.firstname = firstname self.lastname = lastname self.__birth_date = birth_date self.sex = sex self.__class__.add_population()

@classmethod
def add_population(cls): 
    cls.population += 1
   
@property
def birth_date(self): 
    return self.__birth_date

@property
def age(self): 
    return (datetime.date.today() - self.birth_date).days // 365

@birth_date.definer 
def birth_date(self, new_date): 
    self.__birth_date = new_date
    
def __str__(self): 
return f'[{self.firstname} {self.lastname}] birth date: {self.birth_date}'
   
def eat(self, food): 
print(f'{self.firstname} ate {food}, yummy!')

def sleep(self, bed):
print(f'Today {self.firstname} sleeping on the {bed}')

def speak(self, words): 
print(f'{self.firstname} told {words}')

def walk(self, steps):
print(f' Today  {self.firstname} made {steps} steps')

def stand(self, spot):
print(f'{self.firstname} stopped at the {spot}')

def lie(self, place):
print(f'{self.firstname} lying {place}')

def __repr__(self):
return f'Human being {self} has next param: {self.firstname} {self.lastname}'

if __name__ == "__main__":
exper = Human('Albert', 'Einstein', 'male', birth_date=datetime.date(1879, 3, 14)) 
print(exper) 
print(exper.firstname, 'is', exper.age) 
exper.birth_date = datetime.date(1879, 3, 14) 
print(exper) 
print(exper.firstname, 'is', exper.age 'years old') 
print('Human population: ', Human.population)
