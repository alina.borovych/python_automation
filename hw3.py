#Task1 (how to check if the seq_number is not bigger than user_message number of letters)
user_message = input ("Enter your word:")
if not user_message:
     print("Please, write something!")
     quit()
elif not user_message.isalpha():
     print("Please, write only letters")
     quit()
seq_number = True
try:
     seq_number = int(input ("Enter the number of the letter:"))
except ValueError:
     print("Please, enter something next time")
     quit()
# if len(seq_number) > len(user_message):                   ////object of type 'int' has no len()
#     print("The word doesn't have so many letters")
#     quit()
if seq_number == 0:
    print("Enter another number")
    quit()
letter_output = user_message[seq_number-1]
print('The', seq_number, 'letter in word', user_message, 'is', letter_output)



#Task 2 
user_sentence = input("Enter your sentence: ")
word_list = user_sentence.split()
if not user_sentence:
    print("Please, enter something!")
    quit()
contains_digit = any(map(str.isdigit, user_sentence))
if contains_digit == True:
     print("Please, enter a sentance without numbers next time")
     quit()
number_of_words = len(word_list)
print("Number of words in your sentence is", number_of_words) 



#Task3
lst_1 = ['1', '2', '5', '7', 'True', 'False', 3, 9, 15, 0.03, 'Cucumber', 'bts', 0.000001,]
print(lst_1)
lst_2 = [num for num in lst_1 if isinstance(num, (int,float))]
print(lst_2)

#task3 try2 (why doesn't work with input)
input_string = input ('Enter your list: ')
user_list = input_string.split()
print('list: ', user_list)
lst_2 = [num for num in user_list if isinstance(num, (int,float))]
print(lst_2)









