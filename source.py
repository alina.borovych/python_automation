MAX_AGE = 120


def get_user_input() -> str:
    while True:
        raw_input = input(f'Input your age here (below {MAX_AGE}) >>> ').lstrip('0')
        if raw_input.isdigit() and int(raw_input) < MAX_AGE:
            return raw_input


def is_beautiful_age(age: str) -> bool:
    if len(age) > 1 and (age.count(age[0]) == len(age)):
        return True
    else:
        return False


def years(age: int) -> str:
    if age == 1:
        result = 'рік'
    elif age < 5:
        result = 'роки'
    elif age < 21:
        result = 'років'
    elif age % 10 == 1:
        result = 'рік'
    elif str(age)[-1] in {'2', '3', '4'}:
        result = 'роки'
    else:
        result = 'років'
    return result


def main():
    age_str = get_user_input()
    age = int(age_str)
    peculiar_age = is_beautiful_age(age_str)
    year = years(age)

    if peculiar_age:
        response = f'О, вам {age} {year}! Який цікавий вік!'
    elif age < 7:
        response = f'Тобі ж {age} {year}! Де твої батьки?'
    elif age < 16:
        response = f'Тобі лише {age} {year}, а це фільм для дорослих!'
    elif age > 65:
        response = f'Вам {age} {year}? Покажіть пенсійне посвідчення!'
    else:
        response = f'Незважаючи на те що вам {age} {year}, білетів всеодно нема!'
    return response

