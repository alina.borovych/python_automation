def fun(obj, coll) -> list:
    arg_coll = list() 
    for item in coll: 
        arg_coll.append(obj(item) / (2 * item))
    return arg_coll

import math

if __name__ == '__main__':
    collect = (0.03, 0.08, 1, 2, 4, 5, 7, 9, 10, 193, 3333) 
    result = fun(math.log10, collect)
    print('Output: ', result)