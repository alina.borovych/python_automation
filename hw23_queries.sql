-- Сменить пароль пользователю postgres
ALTER USER postgres WITH PASSWORD 'postgres';

-- Создать базу данных store
CREATE DATABASE store;

-- Созать таблицу products c колонками (id, name, price)
\c store
CREATE TABLE products (id VARCHAR(5) PRIMARY KEY, name VARCHAR(32), price VARCHAR(15));

-- Создать таблицу orders с колонками (id, product_id, quontity)
CREATE TABLE orders (id VARCHAR(5) PRIMARY KEY, product_id VARCHAR(10), quantity VARCHAR(10));

-- Заполнить таблицу продуктов записями
INSERT INTO products (id, name, price) VALUES
('30f4', 'Canon EOS M50 Mark II', '31999'),
('11ed', 'Sony Alpha а7 IV', '109999'),
('a261', 'Panasonic Lumix DC-FZ82', '11999'),
('90f5', 'Pentax K100D', '19006'),
('17yd', 'Fujifilm Instax Square SQ 1', '6590'),
('m261', 'Olympus E-PL8 Body', '25386'),
('30l3', 'Nikon Z7', '147999'),
('df5f', 'Kodak M35', '2890');

-- Заполнить таблицу заказавов заказами которые соответствую товарам
INSERT INTO ORDERS (id, product_id, quantity) VALUES
('30c8', 'a261', '3'),
('3k99', '17yd', '15'),
('3d65', '11ed', '1'),
('3m76', '90f5', '2'),
('3c73', '30l3', '1');

-- Сделать выборку в которой есть слфедующие колонки (name, price, quontity, total) total = quontity * price
SELECT name, price, quantity FROM products INNER JOIN orders ON products.id = orders.product_id;
